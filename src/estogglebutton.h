#ifndef CESTOGGLEBUTTON_H
#define CESTOGGLEBUTTON_H
#include <wx/wx.h>
#include <wx/tglbtn.h>

class CESToggleButton : public wxToggleButton
{
public:
   //CESToggleButton();
   CESToggleButton (wxWindow *parent, wxWindowID id, const wxString &label, const wxPoint &pos=wxDefaultPosition, const wxSize &size=wxDefaultSize, long style=wxBU_EXACTFIT, const wxValidator &val=wxDefaultValidator, const wxString &name=wxCheckBoxNameStr);
   virtual ~CESToggleButton();

};

#endif // CESTOGGLEBUTTON_H
