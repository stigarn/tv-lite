#ifndef CXTREAMDLG_H
#define CXTREAMDLG_H

#ifdef __WXMSW__
#include "gui-windows.h"
#else
#include "gui.h"
#endif
#include <wx/string.h>
#include "subscriptioninfo.h"

namespace tvlite
{
enum EPlaylistType
{
   PT_M3UPLUS = 0,
   PT_M3U
};

enum EOutputType
{
   OT_MPEGTS = 0,
   OT_HLS,
   OT_RTMP
};



class CXtreamDlg : public CXtreamDlgBase
{
private:
   CXtreamDlg(const CXtreamDlg& rhs) = delete;
   CXtreamDlg& operator=(const CXtreamDlg& rhs) = delete;

public:
   CXtreamDlg() = delete;
   CXtreamDlg(wxWindow *parent);
   virtual ~CXtreamDlg();
   void GetData(CSubscriptionInfo *info);
   bool ValidateData();
};

}

#endif // CXTREAMDLG_H
