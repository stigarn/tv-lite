#include "main.h"
#include "baseprotocolhandler.h"
#include "debug.h"
#include "userdataobject.h"

using namespace tvlite;

CBaseProtocolHandler::CBaseProtocolHandler(wxEvtHandler *parent, wxString url, wxString name, wxArrayString vlcoptions) :
    m_vlcPlayer(NULL),
    m_pparent(parent),
    m_url(url),
    m_name(name),
    m_vlcoptions(vlcoptions),
    m_stopped(false),
    m_type(0)
{
    DBG_INFO ("Protocol handler type is 0");
}

CBaseProtocolHandler::CBaseProtocolHandler(wxEvtHandler *parent):
    m_vlcPlayer(NULL),
    m_pparent(parent),
    m_url(""),
    m_name(""),
    m_vlcoptions(wxArrayString()),
    m_stopped(false)
{
}


CBaseProtocolHandler::~CBaseProtocolHandler()
{
}

void CBaseProtocolHandler::SetURL(wxString url)
{
   m_url = url;
}

wxString CBaseProtocolHandler::GetURL()
{
   return m_url;
}


void CBaseProtocolHandler::SetName(wxString name)
{
   m_name = name;
}

wxString CBaseProtocolHandler::GetName()
{
   return m_name;
}

void CBaseProtocolHandler::SetVLCOptions(wxArrayString vlcoptions)
{
   m_vlcoptions = vlcoptions;
}

wxArrayString CBaseProtocolHandler::GetVLCOptions()
{
   return m_vlcoptions;
}

void CBaseProtocolHandler::SetVLCPlayer(CVlcPlayer *vlcPlayer)
{
   m_vlcPlayer = vlcPlayer;
}

CVlcPlayer * CBaseProtocolHandler::GetVLCPlayer()
{
   return m_vlcPlayer;
}

void CBaseProtocolHandler::OnStopAsync()
{
       wxCommandEvent exitevent(protEVT_EXIT);
       int *type = new int;
       *type = m_type;
       exitevent.SetClientObject((wxClientData*) new UserDataObject((void *)type));
       DBG_INFO("Send exit event");
       wxQueueEvent(m_pparent, exitevent.Clone());
}

bool CBaseProtocolHandler::WasStopped()
{
   return m_stopped;
}
