#ifndef CCHANNELINFO_H
#define CCHANNELINFO_H

#ifdef __WXMSW__

#include "gui-windows.h"
#else
#include "gui.h"
#endif

#include "main.h"
#include "channel.h"

namespace tvlite
{

class CChannelInfo : public CChannelInfoBase
{
private:
   CChannelInfo(const CChannelInfo& rhs);
   CChannelInfo& operator=(const CChannelInfo& rhs);   
public:
   CChannelInfo();
   CChannelInfo(wxWindow* parent);
   void SetData(CChannel *channel);
   virtual ~CChannelInfo();
protected:
   virtual void OnOkClicked( wxCommandEvent& event );

};

}

#endif // CCHANNELINFO_H
