#include <wx/uri.h>
#include "xtreamdlg.h"

using namespace tvlite;

CXtreamDlg::CXtreamDlg(wxWindow *parent):CXtreamDlgBase(parent)
{

}

CXtreamDlg::~CXtreamDlg()
{

}


void CXtreamDlg::GetData(CSubscriptionInfo *info)
{
   /* http://url:port/get.php?username=xxxx&password=xxxx&type=m3u_plus&output=ts */
   
   wxString sHostPort = m_HostPortText->GetValue().Trim().Trim(false);
   wxString sUsername = m_usernameText->GetValue().Trim().Trim(false);
   wxString sPassword = m_passwordText->GetValue();
   wxString sDataType;
   switch(m_typeRadioBox->GetSelection())
   {
      case PT_M3UPLUS:
         sDataType = "m3u_plus";
         break;
      case PT_M3U:
         sDataType = "m3u";
         break;
      default:
         break;
   }
   wxString sOutputType;
   switch(m_outputRadioBox->GetSelection())
   {
      case OT_MPEGTS:
         sOutputType = "ts";
         break;
      case OT_HLS:  
         sOutputType = "hls";
         break;
      case OT_RTMP:
         sOutputType = "rtmp"; 
      default:
         break;   
   }
   wxURI uURI(sHostPort + "/get.php?username=" + sUsername 
                        + "&password=" + sPassword 
                        + "&type="     + sDataType
                        + "&output="   + sOutputType);
   
   info->name = m_NameText->GetValue().Trim().Trim(false);
   info->url = uURI.BuildUnescapedURI(); 
}

bool CXtreamDlg::ValidateData()
{
   return true;
}