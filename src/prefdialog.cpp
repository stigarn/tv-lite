
#include "prefdialog.h"
#include "configuration.h"
#include <wx/filefn.h>
#include <wx/msgdlg.h>
#include <wx/filedlg.h>
#include <wx/dirdlg.h>

using namespace tvlite;

CPrefDialog::CPrefDialog(wxWindow* parent):CPrefDialogBase(parent)
{
   m_notebook->ChangeSelection(0);
   m_sdbSizer5OK->SetDefault();
}

CPrefDialog::~CPrefDialog()
{
}

void CPrefDialog::SetData()
{
   //general tab
   *m_recordPathTextCtrl << GENERALCONFIG.GetRecordDir();
   m_streamPortSpinCtrl->SetValue((int)GENERALCONFIG.GetStreamPort());
   m_notifCheck->SetValue(GENERALCONFIG.GetNotifEnabled());
   m_updateallcheck->SetValue(GENERALCONFIG.GetUpdateAllEnabled());
   m_playlastcheck->SetValue(GENERALCONFIG.GetPlayLastStreamEnabled());
   //vlc tab
   m_textCtrl13->Disable();
   if (VLCCONFIG.GetRender() == "none")
   {
      m_radioBox4->SetSelection(RBNONE);
   }
   else if (VLCCONFIG.GetRender() == "any")
   {
      m_radioBox4->SetSelection(RBANY);
   }
   else
   {
      m_radioBox4->SetSelection(RBCUSTOM);
      m_textCtrl13->Enable();
   }
   m_textCtrl13->SetValue(VLCCONFIG.GetRender());
   //acestream tab
   *m_aceExecPathTextCtrl << ACESTREAMCONFIG.GetFullPath();
   m_aceMaxCacheSizeSpinCtrl->SetValue(ACESTREAMCONFIG.GetCacheSize()/1048576L);
#ifndef __WXMSW__
   m_CacheTypeRB->SetSelection((unsigned int)ACESTREAMCONFIG.GetCacheType());
#endif
   //network Tab
   bool proxyCheckState = NETWORKCONFIG.GetProxyUsed();
   bool proxyAuthState  = NETWORKCONFIG.GetProxyAuthUsed();
   m_proxyCheck->SetValue(proxyCheckState);
   m_authCheck->SetValue(proxyAuthState);
   *m_addressText << NETWORKCONFIG.GetProxyAddress();
   m_proxyTypeChoice->SetSelection(NETWORKCONFIG.GetProxyType());
   *m_portText << NETWORKCONFIG.GetProxyPort();
   *m_userText << NETWORKCONFIG.GetProxyUserName();
   *m_passwordText << NETWORKCONFIG.GetProxyPassword();
   CheckAuthUsage(proxyAuthState);
   CheckProxyUsage(proxyCheckState);

}

void CPrefDialog::GetData()
{
    GENERALCONFIG.SetRecordDir(m_recordPathTextCtrl->GetValue());
    GENERALCONFIG.SetStreamPort(m_streamPortSpinCtrl->GetValue());
    GENERALCONFIG.SetNotifEnabled(m_notifCheck->GetValue());
    GENERALCONFIG.SetUpdateAllEnabled(m_updateallcheck->GetValue());
    GENERALCONFIG.SetPlayLastStreamEnabled(m_playlastcheck->GetValue());
    switch (m_radioBox4->GetSelection())
    {
       case RBNONE:
         VLCCONFIG.SetRender("none");
         break;
       case RBANY:
         VLCCONFIG.SetRender("any");
         break;
       case RBCUSTOM:
         VLCCONFIG.SetRender(m_textCtrl13->GetValue());
         break;
       default:
         break;
    }
    ACESTREAMCONFIG.SetFullPath(m_aceExecPathTextCtrl->GetValue());
    ACESTREAMCONFIG.SetCacheSize((long)m_aceMaxCacheSizeSpinCtrl->GetValue() * 1048576L);
#ifndef __WXMSW__
    ACESTREAMCONFIG.SetCacheType((eAceCacheType)m_CacheTypeRB->GetSelection());
#endif
    NETWORKCONFIG.SetProxyUsed(m_proxyCheck->GetValue());
    NETWORKCONFIG.SetProxyAuthUsed(m_authCheck->GetValue());
    NETWORKCONFIG.SetProxyType((eProxyType)(m_proxyTypeChoice->GetCurrentSelection()));
    long value;
    if (m_portText->GetValue().ToLong(&value))
    {
      NETWORKCONFIG.SetProxyPort((unsigned short)value);
    }
    NETWORKCONFIG.SetProxyAddress(m_addressText->GetValue());
    NETWORKCONFIG.SetProxyUserName(m_userText->GetValue());
    NETWORKCONFIG.SetProxyPassword(m_passwordText->GetValue());
}

bool CPrefDialog::ValidateData()
{
   bool rc = true;
   wxString recordPath = m_recordPathTextCtrl->GetValue();
   if (!wxDirExists(recordPath))
   {
      wxMessageBox(_("Please input a valid recording path"), _("Validation error"), wxOK|wxICON_STOP, this);
      rc = false;
   }
   if (rc)
   {
      if (m_textCtrl13->GetValue() == "" && m_radioBox4->GetSelection() == RBCUSTOM)
      {
         wxMessageBox(_("Please provide HW acceleration type"), _("Validation error"), wxOK|wxICON_STOP, this);
         rc = false;
      }
   }
//   if (rc)
//   {
//         if (!wxFileExists(m_aceExecPathTextCtrl->GetValue()))
//         {
//            wxMessageBox(_("Please provide the Acestream executable path"), _("Validation error"), wxOK|wxICON_STOP, this);
//            rc = false;
//         }
//   }
   if (m_proxyCheck->GetValue())
   {
      long portValue;
      if (rc)
      {
         if (!m_portText->GetValue().ToLong(&portValue))
         {
             wxMessageBox(_("Please input a valid proxy port value"), _("Validation error"), wxOK|wxICON_STOP, this);
             rc = false;
         }
      }
      if (rc)
      {
         if (1 > portValue || 65535 < portValue)
         {
            wxMessageBox(_("Please input a valid proxy port value"), _("Validation error"), wxOK|wxICON_STOP, this);
            rc = false;
         }
      }
      if (rc)
      {
         if (m_addressText->GetValue() == "")
         {
            wxMessageBox(_("Please input a proxy address"), _("Validation error"), wxOK|wxICON_STOP, this);
            rc = false;
         }
      }
      if (m_authCheck->GetValue())
      {
         if (rc)
         {
            if (m_userText->GetValue() == "")
            {
               wxMessageBox(_("Please input a proxy username"), _("Validation error"), wxOK|wxICON_STOP, this);
               rc = false;
            }
         }
      }
   }

   return rc;
}

void CPrefDialog::OnOkClicked( wxCommandEvent& event )
{
   if (ValidateData())
   {
      EndModal(wxID_OK);
   }
}

void CPrefDialog::OnRecordingPathClick( wxCommandEvent& event )
{
    wxDirDialog openDirDialog(this, _("Choose Recording Folder"));
    if (openDirDialog.ShowModal() == wxID_CANCEL)
    {
       return;
    }
    m_recordPathTextCtrl->SetValue(openDirDialog.GetPath());
}

void CPrefDialog::OnHwAccType( wxCommandEvent& event )
{
   if (m_radioBox4->GetSelection() == RBCUSTOM)
   {
      m_textCtrl13->Enable();
      m_textCtrl13->SetValue("");
   }
   else if (m_radioBox4->GetSelection() == RBNONE)
   {
      m_textCtrl13->SetValue("none");
      m_textCtrl13->Enable(false);
   }
   else if (m_radioBox4->GetSelection() == RBANY)
   {
      m_textCtrl13->SetValue("any");
      m_textCtrl13->Enable(false);
   }
}

void CPrefDialog::OnAcestreamExecPathClick( wxCommandEvent& event )
{
    wxFileDialog openFileDialog(this, _("Choose Acestream file"), "", "",
                       wxFileSelectorDefaultWildcardStr, wxFD_OPEN|wxFD_FILE_MUST_EXIST);
    if (openFileDialog.ShowModal() == wxID_CANCEL)
    {
       return;
    }
    m_aceExecPathTextCtrl->SetValue(openFileDialog.GetPath());
}

void CPrefDialog::OnProxyUseClick (wxCommandEvent& event)
{
   CheckProxyUsage(m_proxyCheck->GetValue());
}

void CPrefDialog::OnProxyAuthCheck(wxCommandEvent& event)
{
   CheckAuthUsage(m_authCheck->GetValue());
}

void CPrefDialog::CheckProxyUsage(bool state)
{
   m_addressText->Enable(state);
   m_proxyTypeChoice->Enable(state);
   m_portText->Enable(state);
   m_authCheck->Enable(state);
   m_userText->Enable(state && m_authCheck->GetValue());
   m_passwordText->Enable(state && m_authCheck->GetValue());

}

void CPrefDialog::CheckAuthUsage(bool state)
{
   m_userText->Enable(state);
   m_passwordText->Enable(state);
}

