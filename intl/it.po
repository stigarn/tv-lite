msgid ""
msgstr ""
"Project-Id-Version: TV-Lite 0.1.0\n"
"POT-Creation-Date: 2020-06-04 21:45+0300\n"
"PO-Revision-Date: 2020-06-16 10:03+0200\n"
"Language-Team: Cristian Burneci\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Poedit 2.3\n"
"X-Poedit-Basepath: ../src\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Poedit-SourceCharset: UTF-8\n"
"X-Poedit-KeywordsList: _\n"
"Last-Translator: \n"
"Language: it\n"
"X-Poedit-SearchPath-0: .\n"

#: TVLite.desktop:5 gui.h:107
msgid "TV-Lite"
msgstr "TV=Lite"

#: TVLite.desktop:8
msgid "TV online viewer"
msgstr "Televisione in rete"

#: aceprotocol.cpp:34
msgid ""
"Could not search for the Acestream binary.\n"
" Check your installation!"
msgstr "Binary Acestream non trovato. Controllare l'installazione !"

#: aceprotocol.cpp:34 aceprotocol.cpp:41 aceprotocol.cpp:58
#: acestreamprotocolhandler.cpp:63 acestreamprotocolhandler.cpp:137
#: main.cpp:273 main.cpp:859 sopprotocol.cpp:86 sopprotocol.cpp:92
#: sopprotocol.cpp:108 subscription.cpp:107 subscription.cpp:153
#: subscriptiondialog.cpp:58 subscriptionlistdialog.cpp:57
msgid "Error"
msgstr "Errore"

#: aceprotocol.cpp:41
msgid "Could not find the Acestream engine binary in the path."
msgstr "Bynari Acestream non trovato nella path."

#: aceprotocol.cpp:58
msgid "Could not execute Acestream engine!"
msgstr "Non e possibile partire il motore Acestream !"

#: acestreamprotocolhandler.cpp:63 acestreamprotocolhandler.cpp:137
msgid "Error retrieving data from Acestream engine"
msgstr "Errore durante il recupero dei dati dal motore Acestream"

#: channeledit.cpp:48 localdialog.cpp:34
msgid "Please input a valid name"
msgstr "Si prega di inserire un nome valido"

#: channeledit.cpp:48 channeledit.cpp:62 localdialog.cpp:34
msgid "Validation error"
msgstr "Errore di convalida"

#: channeledit.cpp:62
msgid "Please input at least a valid address"
msgstr "Inserire almeno un indirizzo valido"

#: gui.cpp:21
msgid "Manage &Lists..."
msgstr "Gestisci &elenchi ..."

#: gui.cpp:23
msgid "Add Local List"
msgstr "Aggiungi elenco locale"

#: gui.cpp:29
msgid "Save Local List As..."
msgstr "Salva elenco locale come ..."

#: gui.cpp:33
msgid "Save Subscription As Local List"
msgstr "Salva abbonamento come elenco locale"

#: gui.cpp:38
msgid "Manage Local Lists"
msgstr "Gestisci elenchi locali"

#: gui.cpp:41
msgid "Manage Subscriptions"
msgstr "Gestisci le iscrizioni"

#: gui.cpp:47
msgid "&Options"
msgstr "&Opzioni"

#: gui.cpp:53
msgid "E&xit"
msgstr "Uscita"

#: gui.cpp:56
msgid "&TVLite"
msgstr "&TvLite"

#: gui.cpp:60
msgid "Full Screem"
msgstr "A schermo intero"

#: gui.cpp:63
msgid "Channel List"
msgstr "Elenco canali"

#: gui.cpp:70
msgid "Aspect Ratio"
msgstr "Proporzioni"

#: gui.cpp:72
msgid "Default"
msgstr "Predefinito"

#: gui.cpp:76
msgid "16:9"
msgstr "16:9"

#: gui.cpp:80
msgid "4:3"
msgstr "4:3"

#: gui.cpp:85
msgid "&View"
msgstr "&Visualizza"

#: gui.cpp:89
msgid "About"
msgstr "Info"

#: gui.cpp:92
msgid "Help"
msgstr "Aiuto"

#: gui.cpp:106 gui.h:141
msgid "Local Lists"
msgstr "Elenco loca;e"

#: gui.cpp:106 gui.h:170
msgid "Subscriptions"
msgstr "Sottoscrizioni"

#: gui.cpp:108
msgid "List Types"
msgstr "Tipi di elenco"

#: gui.cpp:112
msgid "Channel List:"
msgstr "Elenco canali:"

#: gui.cpp:124
msgid "Total:"
msgstr "Soma:"

#: gui.cpp:257 gui.cpp:313
msgid "New"
msgstr "Nuovo"

#: gui.cpp:260 gui.cpp:322
msgid "Edit"
msgstr "Modificare"

#: gui.cpp:263 gui.cpp:325
msgid "Delete"
msgstr "Cancella"

#: gui.cpp:272 gui.cpp:334
msgid "Close"
msgstr "Chiudi"

#: gui.cpp:319 subscriptionlistdialog.cpp:68
msgid "Update"
msgstr "Aggiornare"

#: gui.cpp:368 gui.cpp:428 subscriptionlistdialog.cpp:9
msgid "Name"
msgstr "Nome"

#: gui.cpp:375 gui.cpp:442 subscriptionlistdialog.cpp:10
msgid "URL"
msgstr "URL"

#: gui.cpp:385
msgid " ... "
msgstr " ... "

#: gui.cpp:435
msgid "Author"
msgstr "Autore"

#: gui.cpp:449
msgid "Version"
msgstr "Versione"

#: gui.cpp:490
msgid "Channel Name"
msgstr "Nome del Canale"

#: gui.cpp:497
msgid "Channel Addresses"
msgstr "Indirizzo del Canale"

#: gui.h:200
msgid "Path"
msgstr "Path"

#: gui.h:231
msgid "Edit Local List"
msgstr "Modifica elenco locale"

#: locallistdialog.cpp:73 subscriptionlistdialog.cpp:81
msgid "Do you really wish to delete the selected subscription, "
msgstr "Desideri davvero eliminare l'abbonamento selezionato, "

#: locallistdialog.cpp:74 subscriptionlistdialog.cpp:82
msgid "Question"
msgstr "Domande"

#: main.cpp:77
msgid "Could not initialize program configuration"
msgstr "Impossibile inizializzare la configurazione del programma"

#: main.cpp:107 main.cpp:312
msgid "Ready"
msgstr "Pronto"

#: main.cpp:273
msgid "Acestream engine not found"
msgstr "Motore Acestream non trovato"

#: main.cpp:516 main.cpp:536
msgid "New Item"
msgstr "Nuovo oggetto"

#: main.cpp:537
msgid "Edit Item"
msgstr "Modifica oggetto"

#: main.cpp:538
msgid "Delete Item"
msgstr "Cancella oggetto"

#: main.cpp:771
msgid "The list was saved."
msgstr "L'elenco è stato salvato."

#: main.cpp:790
msgid "Save File As _?"
msgstr "Salva file come _?"

#: main.cpp:791
msgid "TV-Maxe lists (*.db)|*.db|All files (*)|*"
msgstr "Elenchi TV-Maxe (* .db) | * .db | Tutti i file (*) | *"

#: main.cpp:817
msgid "Viewer for online TV Channels - TV-maxe like"
msgstr "Visualizzatore per canali TV online - uguale a TV-maxe"

#: main.cpp:842
msgid "Buffering"
msgstr "Buffering"

#: main.cpp:847
msgid "Playing"
msgstr "Partenza"

#: main.cpp:851
msgid "Paused"
msgstr "Pausa"

#: main.cpp:854
msgid "Stopped"
msgstr "Fermato"

#: main.cpp:859
msgid "The player encountered an error while trying to play URL"
msgstr "Il ripprodutore ha riscontrato un errore durante il tentativo di riprodurre l'URL"

#: sopprotocol.cpp:86
msgid ""
"Could not search for the Sopcast binary.\n"
" Check your installation!"
msgstr "Impossibile cercare il binario Sopcast. Controlla la tua installazione!"

#: sopprotocol.cpp:92
msgid "Could not find the Sopcast binary in the path."
msgstr "Impossibile trovare il binario Sopcast nel percorso."

#: sopprotocol.cpp:108
msgid "Could not execute Sopcast!"
msgstr "Impossibile eseguire Sopcast!"

#: sopprotocolhandler.cpp:40
msgid "Starting Sopcast"
msgstr "Avvio di Sopcast"

#: subscription.cpp:107
msgid "Error downloading channel list"
msgstr "Errore durante il download dell'elenco dei canali"

#: subscription.cpp:153
msgid "Format not recognized"
msgstr "Formato non riconosciuto"

#: subscriptiondialog.cpp:58
msgid "Found another subscription with the same URL!"
msgstr "Trovato un altro abbonamento con lo stesso URL!"

#: subscriptiondialog.cpp:85
msgid "Open XYZ file"
msgstr "Apri il file XYZ"

#: subscriptionlistdialog.cpp:57
msgid "Could not update the selected subscription"
msgstr "Impossibile aggiornare l'abbonamento selezionato"

#: subscriptionlistdialog.cpp:68
msgid "Subscription Updated"
msgstr "Abbonamento aggiornato"
